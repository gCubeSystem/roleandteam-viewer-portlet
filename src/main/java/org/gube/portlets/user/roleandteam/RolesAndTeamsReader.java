package org.gube.portlets.user.roleandteam;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.gcube.common.portal.GCubePortalConstants;
import org.gcube.common.portal.PortalContext;
import org.gcube.portal.databook.client.GCubeSocialNetworking;
import org.gcube.vomanagement.usermanagement.impl.LiferayGroupManager;
import org.gcube.vomanagement.usermanagement.impl.LiferayRoleManager;
import org.gcube.vomanagement.usermanagement.model.GCubeTeam;

import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class RolesAndTeamsReader
 */
public class RolesAndTeamsReader extends MVCPortlet {

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		try {
			String username = PortalUtil.getUser(renderRequest).getScreenName();
			HttpServletRequest httpReq = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest));
			String context = getCurrentContext(renderRequest);
			System.out.println("context="+context);
			long groupId = PortalUtil.getScopeGroupId(renderRequest);
			ArrayList<GroupDTO> groups = getGroups(groupId);
			renderRequest.setAttribute("groups", groups);
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.render(renderRequest, renderResponse);		
	}

	public static String getCurrentContext(RenderRequest request) {
		long groupId = -1;
		try {
			groupId = PortalUtil.getScopeGroupId(request);
			return getCurrentContext(groupId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getCurrentContext(long groupId) {
		try {
			PortalContext pContext = PortalContext.getConfiguration(); 
			return pContext.getCurrentScope(""+groupId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public ArrayList<GroupDTO> getGroups(long vreGroupId ) {		
		ArrayList<GroupDTO> toReturn = new ArrayList<>();	


		try {
			String vreFriendlyURL = new LiferayGroupManager().getGroup(vreGroupId).getFriendlyURL();
			StringBuffer pageToRedirectURL= new StringBuffer(GCubePortalConstants.PREFIX_GROUP_URL)
					.append(vreFriendlyURL)
					.append(GCubePortalConstants.GROUP_MEMBERS_FRIENDLY_URL)
					.append("?")
					.append(new String(Base64.encodeBase64(GCubeSocialNetworking.GROUP_MEMBERS_OID.getBytes())))
					.append("=");		
			//add the View Managers redirect (and -1 as groupID)
			String managerRedirectURL = new String(pageToRedirectURL);
			managerRedirectURL += new String(Base64.encodeBase64(("-100").getBytes()));

			toReturn.add(new GroupDTO(true, "View Managers", "No Desc", managerRedirectURL));
			List<GCubeTeam> groups = new LiferayRoleManager().listTeamsByGroup(vreGroupId);
			for (GCubeTeam g : groups) {
				String encodedTeamId = new String(Base64.encodeBase64((""+g.getTeamId()).getBytes()));
				String teamRedirectURL = pageToRedirectURL+encodedTeamId;
				toReturn.add(new GroupDTO(
						false, 
						g.getTeamName(),
						g.getDescription(), 
						teamRedirectURL));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}




		return toReturn;

	}
}