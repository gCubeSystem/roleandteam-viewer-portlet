
# Changelog for Share-Updates Portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.4.0-SNAPSHOT] - 2022-01-04

- Ported to git

- Feature #22595 VRE Managers and Groups should be extended to show also other roles


## [v2.3.0] - 2018-10-22

- Support #12736, VRE Managers and Groups: text misaligned

- Ported to Java 8 and GWT 2.8.1

## [v2.0.0] - 2016-11-23

- Removed ASL Session, increased performance

- Displays VRE-Managers and Groups in the related VRE

## [v1.0.0] - 2014-12-09

- First release
